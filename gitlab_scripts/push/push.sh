#!/bin/bash

echo "*******************************"
echo "******Pushing the image********"
echo "*******************************"


IMAGE=maven-project

echo "****Logging into Docker Hub****"
sudo docker login -u anantv -p $PASS 

echo "****Tagging the Docker image****"
sudo docker tag $IMAGE:$BUILD_TAG anantv/$IMAGE:$BUILD_TAG

echo "****Pushing the docker image*****"
sudo docker push anantv/$IMAGE:$BUILD_TAG

