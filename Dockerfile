FROM openjdk:8-jre-slim

RUN mkdir /app

COPY *.jar /app/app.jar

CMD java -jar /app/app.jar
